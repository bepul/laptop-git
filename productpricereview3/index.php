<!DOCTYPE html>
<html>
<head>
<title>Product Price Review</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/animate.css">
<link rel="stylesheet" type="text/css" href="assets/css/slick.css">
<link rel="stylesheet" type="text/css" href="assets/css/theme.css">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<!--[if lt IE 9]>
<script src="assets/js/html5shiv.min.js"></script>
<script src="assets/js/respond.min.js"></script>
<![endif]-->
</head>
<body>
<div id="preloader">
  <div id="status">&nbsp;</div>
</div>
<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
<div class="container">
  <header id="header">
	<div class="row">
	  <div class="col-lg-12 col-md-12">
		<div class="header_top">
		  <div class="header_top_left">
			<ul class="top_nav">
			  <li><a href="#">Home</a></li>
			  <li><a href="#">About</a></li>
			  <li><a href="#">Contact</a></li>
			  <li><a href="#">Error Page</a></li>
			  <li><a href="admin/index.php">Admin Login</a></li>
			</ul>
		  </div>
		  <div class="header_top_right">
			<form action="#" class="search_form">
			  <input type="text" placeholder="Text to Search">
			  <input type="submit" value="">
			</form>
		  </div>
		</div>
		<div class="header_bottom">
		  <div class="header_bottom_left"><a class="logo" href="index.php">Product<strong>Price</strong>Review <span>An amazon affiliate website</span></a></div>
		  <div class="header_bottom_right"><a href="#"><img src="images/addbanner_728x90_V1.jpg" alt=""></a></div>
		</div>
	  </div>
	</div>
  </header>
  <div id="navarea">
	<nav class="navbar navbar-default" role="navigation">
	  <div class="container-fluid">
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
		  <ul class="nav navbar-nav custom_nav">
			<li class=""><a href="index.php">Home</a></li>
			<li><a href="pages/electronics.php">Electronics</a></li>            
		  </ul>
		</div>
	  </div>
	</nav>
  </div>
  <section id="mainContent">
	<div class="content_top">
	  <div class="row">
		<div class="col-lg-6 col-md-6 col-sm6">
		  <div class="latest_slider">
			<div class="slick_slider">
			  <div class="single_iteam"><img src="images/550x330x1.jpg" alt="">
				<h2><a class="slider_tittle" href="">Product 1 from Category 1</a></h2>
			  </div>
			  <div class="single_iteam"><img src="images/550x330x2.jpg" alt="">
				<h2><a class="slider_tittle" href="">Product 2 from Category 1</a></h2>
			  </div>              
			</div>
		  </div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm6">
		  <div class="content_top_right">
			<ul class="featured_nav wow fadeInDown">
			  <li><img src="images/300x215x1.jpg" alt="">
				<div class="title_caption"><a href="#">Product 1 from Category 2</a></div>
			  </li>
			  <li><img src="images/300x215x2.jpg" alt="">
				<div class="title_caption"><a href="#">Product 1 from Category 3</a></div>
			  </li>
			  <li><img src="images/300x215x3.jpg" alt="">
				<div class="title_caption"><a href="#">Product 1 from Category 3</a></div>
			  </li>
			  <li><img src="images/300x215x4.jpg" alt="">
				<div class="title_caption"><a href="#">Product 1 from Category 4</a></div>
			  </li>
			</ul>
		  </div>
		</div>
	  </div>
	</div>
	<div class="content_middle">
	  <div class="col-lg-3 col-md-3 col-sm-3">
		<div class="content_middle_leftbar">
		  <div class="single_category wow fadeInDown">
			<h2> <span class="bold_line"><span></span></span> <span class="solid_line"></span> <a href="#" class="title_text">category5</a> </h2>
			<ul class="catg1_nav">
			  <li>
				<div class="catgimg_container"> <a href="#" class="catg1_img"><img alt="" src="images/292x150x1.jpg"></a></div>
				<h3 class="post_titile"><a href="#">Product 1 from Category 5</a></h3>
			  </li>
			  <li>
				<div class="catgimg_container"> <a href="#" class="catg1_img"><img alt="" src="images/292x150x2.jpg"></a></div>
				<h3 class="post_titile"><a href="#">Product 2 from Category 5</a></h3>
			  </li>
			</ul>
		  </div>
		</div>
	  </div>
	  <div class="col-lg-6 col-md-6 col-sm-6">
		<div class="content_middle_middle">
		  <div class="slick_slider2">
			<div class="single_featured_slide"> <a href="#"><img src="images/567x330x1.jpg" alt=""></a>
			  <h2><a href="#">Product 1 from Category 6</a></h2>
			  <p>Nunc tincidunt, elit non cursus euismod, lacus augue ornare metus, egestas imperdiet nulla nisl quis mauris. Suspendisse a pharetra urna. Morbi dui lectus, pharetra nec elementum eget, vulput...</p>
			</div>
			<div class="single_featured_slide"> <a href="#"><img src="images/567x330x2.jpg" alt=""></a>
			  <h2><a href="#">Product 2 from Category 6</a></h2>
			  <p>Nunc tincidunt, elit non cursus euismod, lacus augue ornare metus, egestas imperdiet nulla nisl quis mauris. Suspendisse a pharetra urna. Morbi dui lectus, pharetra nec elementum eget, vulput...</p>
			</div>
			
		  </div>
		</div>
	  </div>
	  <div class="col-lg-3 col-md-3 col-sm-3">
		<div class="content_middle_rightbar">
		  <div class="single_category wow fadeInDown">
			<h2> <span class="bold_line"><span></span></span> <span class="solid_line"></span> <a href="#" class="title_text">category7</a> </h2>
			<ul class="catg1_nav">
			  <li>
				<div class="catgimg_container"> <a href="#" class="catg1_img"><img alt="" src="images/292x150x1.jpg"></a></div>
				<h3 class="post_titile"><a href="#">Product 1 from Category 7</a></h3>
			  </li>
			  <li>
				<div class="catgimg_container"> <a href="#" class="catg1_img"><img alt="" src="images/292x150x2.jpg"></a></div>
				<h3 class="post_titile"><a href="#">Product 2 from Category 7</a></h3>
			  </li>
			</ul>
		  </div>
		</div>
	  </div>
	</div>
	<div class="content_bottom">
	  <div class="col-lg-8 col-md-8">
		<div class="content_bottom_left">
		  <div class="single_category wow fadeInDown">
			<h2> <span class="bold_line"><span></span></span> <span class="solid_line"></span> <a class="title_text" href="#">Category8</a> </h2>
			<div class="business_category_left wow fadeInDown">
			  <ul class="fashion_catgnav">
				<li>
				  <div class="catgimg2_container"> <a href="#"><img alt="" src="images/390x240x1.jpg"></a> </div>
				  <h2 class="catg_titile"><a href="#">Product 1 from Category 8</a></h2>
				  <div class="comments_box"> <span class="meta_date">14/12/2045</span> <span class="meta_comment"><a href="#">No Comments</a></span> <span class="meta_more"><a  href="#">Read More...</a></span> </div>
				  <p>Nunc tincidunt, elit non cursus euismod, lacus augue ornare metus, egestas imperdiet nulla...</p>
				</li>
			  </ul>
			</div>
			<div class="business_category_right wow fadeInDown">
			  <ul class="small_catg">
				<li>
				  <div class="media wow fadeInDown"> <a class="media-left" href="#"><img src="images/112x112.jpg" alt=""></a>
					<div class="media-body">
					  <h4 class="media-heading"><a href="#">Product 2 from Category 8 </a></h4>
					  <div class="comments_box"> <span class="meta_date">14/12/2045</span> <span class="meta_comment"><a href="#">No Comments</a></span> </div>
					</div>
				  </div>
				</li>
				
			  </ul>
			</div>
		  </div>
		  <div class="games_fashion_area">
			<div class="games_category">
			  <div class="single_category">
				<h2> <span class="bold_line"><span></span></span> <span class="solid_line"></span> <a class="title_text" href="#">Category 9</a> </h2>
				<ul class="fashion_catgnav wow fadeInDown">
				  <li>
					<div class="catgimg2_container"> <a href="#"><img alt="" src="images/390x240x1.jpg"></a> </div>
					<h2 class="catg_titile"><a href="#">Product 1 from Category 9</a></h2>
					<div class="comments_box"> <span class="meta_date">14/12/2045</span> <span class="meta_comment"><a href="#">No Comments</a></span> <span class="meta_more"><a  href="#">Read More...</a></span> </div>
					<p>Nunc tincidunt, elit non cursus euismod, lacus augue ornare metus, egestas imperdiet nulla...</p>
				  </li>
				</ul>
				<ul class="small_catg wow fadeInDown">
				  <li>
					<div class="media"> <a class="media-left" href="#"><img src="images/112x112.jpg" alt=""></a>
					  <div class="media-body">
						<h4 class="media-heading"><a href="#">Product 2 from Category 9 </a></h4>
						<div class="comments_box"> <span class="meta_date">14/12/2045</span> <span class="meta_comment"><a href="#">No Comments</a></span> </div>
					  </div>
					</div>
				  </li>
				  <li>
					<div class="media wow fadeInDown"> <a class="media-left" href="#"><img src="images/112x112.jpg" alt=""></a>
					  <div class="media-body">
						<h4 class="media-heading"><a href="#">Product 3 from Category 9 </a></h4>
						<div class="comments_box"> <span class="meta_date">14/12/2045</span> <span class="meta_comment"><a href="#">No Comments</a></span> </div>
					  </div>
					</div>
				  </li>
				</ul>
			  </div>
			</div>
			<div class="fashion_category">
			  <div class="single_category">
				<div class="single_category wow fadeInDown">
				  <h2> <span class="bold_line"><span></span></span> <span class="solid_line"></span> <a class="title_text" href="#">Category 10</a> </h2>
				  <ul class="fashion_catgnav wow fadeInDown">
					<li>
					  <div class="catgimg2_container"> <a href="#"><img alt="" src="images/390x240x1.jpg"></a> </div>
					  <h2 class="catg_titile"><a href="#">Product 1 from Category 10</a></h2>
					  <div class="comments_box"> <span class="meta_date">14/12/2045</span> <span class="meta_comment"><a href="#">No Comments</a></span> <span class="meta_more"><a  href="#">Read More...</a></span> </div>
					  <p>Nunc tincidunt, elit non cursus euismod, lacus augue ornare metus, egestas imperdiet nulla...</p>
					</li>
				  </ul>
				  <ul class="small_catg wow fadeInDown">
					<li>
					  <div class="media wow fadeInDown"> <a class="media-left" href="#"><img src="images/112x112.jpg" alt=""></a>
						<div class="media-body">
						  <h4 class="media-heading"><a href="#">Product 2 from Category 10 </a></h4>
						  <div class="comments_box"> <span class="meta_date">14/12/2045</span> <span class="meta_comment"><a href="#">No Comments</a></span> </div>
						</div>
					  </div>
					</li>
					<li>
					  <div class="media wow fadeInDown"> <a class="media-left" href="#"><img src="images/112x112.jpg" alt=""></a>
						<div class="media-body">
						  <h4 class="media-heading"><a href="#">Product 3 from Category 10 </a></h4>
						  <div class="comments_box"> <span class="meta_date">14/12/2045</span> <span class="meta_comment"><a href="#">No Comments</a></span> </div>
						</div>
					  </div>
					</li>
				  </ul>
				</div>
			  </div>
			</div>
		  </div>
		  <div class="technology_catrarea">
			<div class="single_category">
			  <h2> <span class="bold_line"><span></span></span> <span class="solid_line"></span> <a class="title_text" href="#">Category 11</a> </h2>
			  <div class="business_category_left">
				<ul class="fashion_catgnav wow fadeInDown">
				  <li>
					<div class="catgimg2_container"> <a href="#"><img alt="" src="images/390x240x1.jpg"></a> </div>
					<h2 class="catg_titile"><a href="#">Product 1 from Category 11</a></h2>
					<div class="comments_box"> <span class="meta_date">14/12/2045</span> <span class="meta_comment"><a href="#">No Comments</a></span> <span class="meta_more"><a  href="#">Read More...</a></span> </div>
					<p>Nunc tincidunt, elit non cursus euismod, lacus augue ornare metus, egestas imperdiet nulla...</p>
				  </li>
				</ul>
			  </div>
			  <div class="business_category_right">
				<ul class="small_catg wow fadeInDown">
				  <li>
					<div class="media wow fadeInDown"> <a class="media-left" href="#"><img src="images/112x112.jpg" alt=""></a>
					  <div class="media-body">
						<h4 class="media-heading"><a href="#">Product 2 from Category 11 </a></h4>
						<div class="comments_box"> <span class="meta_date">14/12/2045</span> <span class="meta_comment"><a href="#">No Comments</a></span> </div>
					  </div>
					</div>
				  </li>
				  <li>
					<div class="media wow fadeInDown"> <a class="media-left" href="#"><img src="images/112x112.jpg" alt=""></a>
					  <div class="media-body">
						<h4 class="media-heading"><a href="#">Product 3 from Category 11 </a></h4>
						<div class="comments_box"> <span class="meta_date">14/12/2045</span> <span class="meta_comment"><a href="#">No Comments</a></span> </div>
					  </div>
					</div>
				  </li>
				  <li>
					<div class="media wow fadeInDown"> <a class="media-left" href="#"><img src="images/112x112.jpg" alt=""></a>
					  <div class="media-body">
						<h4 class="media-heading"><a href="#">Product 4 from Category 11 </a></h4>
						<div class="comments_box"> <span class="meta_date">14/12/2045</span> <span class="meta_comment"><a href="#">No Comments</a></span> </div>
					  </div>
					</div>
				  </li>
				</ul>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	  <div class="col-lg-4 col-md-4">
		<div class="content_bottom_right">
		  <div class="single_bottom_rightbar">
			<h2>Recent Post</h2>
			<ul class="small_catg popular_catg wow fadeInDown">
			  <li>
				<div class="media wow fadeInDown"> <a href="#" class="media-left"><img alt="" src="images/112x112.jpg"> </a>
				  <div class="media-body">
					<h4 class="media-heading"><a href="#">Random Product 1</a></h4>
					<p>Nunc tincidunt, elit non cursus euismod, lacus augue ornare metus, egestas imperdiet nulla nisl quis mauris. Suspendisse a pharetra </p>
				  </div>
				</div>
			  </li>
			  <li>
				<div class="media wow fadeInDown"> <a href="#" class="media-left"><img alt="" src="images/112x112.jpg"> </a>
				  <div class="media-body">
					<h4 class="media-heading"><a href="#">Random Product 2 </a></h4>
					<p>Nunc tincidunt, elit non cursus euismod, lacus augue ornare metus, egestas imperdiet nulla nisl quis mauris. Suspendisse a pharetra </p>
				  </div>
				</div>
			  </li>
			  <li>
				<div class="media wow fadeInDown"> <a href="#" class="media-left"><img alt="" src="images/112x112.jpg"> </a>
				  <div class="media-body">
					<h4 class="media-heading"><a href="#">Random Product  3 </a></h4>
					<p>Nunc tincidunt, elit non cursus euismod, lacus augue ornare metus, egestas imperdiet nulla nisl quis mauris. Suspendisse a pharetra </p>
				  </div>
				</div>
			  </li>
			</ul>
		  </div>
		  
		
		  
		</div>
	  </div>
	</div>
  </section>
</div>
<footer id="footer">
  <div class="footer_top">
	<div class="container">
	  <div class="row">
		<div class="col-lg-4 col-md-4 col-sm-4">
		  <div class="single_footer_top wow fadeInLeft">
			<h2>Flicker Images</h2>
			<ul class="flicker_nav">
			  <li><a href="#"><img src="images/75x75.jpg" alt=""></a></li>
			  <li><a href="#"><img src="images/75x75.jpg" alt=""></a></li>
			  <li><a href="#"><img src="images/75x75.jpg" alt=""></a></li>
			  <li><a href="#"><img src="images/75x75.jpg" alt=""></a></li>
			  <li><a href="#"><img src="images/75x75.jpg" alt=""></a></li>
			  <li><a href="#"><img src="images/75x75.jpg" alt=""></a></li>
			  <li><a href="#"><img src="images/75x75.jpg" alt=""></a></li>
			  <li><a href="#"><img src="images/75x75.jpg" alt=""></a></li>
			</ul>
		  </div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4">
		  <div class="single_footer_top wow fadeInDown">
			<h2>Labels</h2>
			<ul class="labels_nav">
			  <li><a href="#">Gallery</a></li>
			  <li><a href="#">Business</a></li>
			  <li><a href="#">Games</a></li>
			  <li><a href="#">Fashion</a></li>
			  <li><a href="#">Sports</a></li>
			  <li><a href="#">Technology</a></li>
			  <li><a href="#">Slider</a></li>
			  <li><a href="#">Life &amp; Style</a></li>
			</ul>
		  </div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4">
		  <div class="single_footer_top wow fadeInRight">
			<h2>About Us</h2>
			<p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed nec laoreet orci, eget ullamcorper quam. Phasellus lorem neque, </p>
		  </div>
		</div>
	  </div>
	</div>
  </div>
  <div class="footer_bottom">
	<div class="container">
	  <div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		  <div class="footer_bottom_left">
			<p>Copyright &copy; 2045 <a href="index.php">Prodcut Price and Review</a></p>
		  </div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		  <div class="footer_bottom_right">
			<p>Developed BY Prodcut Price and Review</p>
		  </div>
		</div>
	  </div>
	</div>
  </div>
</footer>
<script src="assets/js/jquery.min.js"></script> 
<script src="assets/js/bootstrap.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/slick.min.js"></script> 
<script src="assets/js/custom.js"></script>
</body>
</html>